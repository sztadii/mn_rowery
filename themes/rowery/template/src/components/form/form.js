var form;

form = new function () {

  //catch DOM
  var $el;
  var $form;
  var $textarea;
  var $inputs;

  //bind events
  $(document).ready(function () {
    init()
  });

  //public functions
  var init = function () {
    $el = $('.form');
    $form = $el.find('.form__content');
    $textarea = $el.find('.form__textarea');
    $inputs = $el.find('.form__input');

    watchForm();
  };

  //private functions
  var watchForm = function () {
    $textarea.on("focus", function () {
      $(this).addClass("-visited")
    });

    $inputs.bind('change blur', (function () {
      if ($(this).is(":valid")) {
        $(this).removeClass("-invalid")
      } else {
        $(this).addClass("-invalid")
      }
    }));

    $inputs.focus(function () {
      $(this).removeClass("-invalid");
    });

    $inputs.on("invalid", function () {
      $(window).width() <= 1023 && $inputs.trigger("blur")
    })
  };
};