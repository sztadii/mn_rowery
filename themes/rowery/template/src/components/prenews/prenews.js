var prenews;

prenews = new function () {

  //catch DOM
  var $el;
  var $slider;

  //bind events
  $(document).ready(function () {
    init();
  });

  //private functions
  var init = function () {
    $el = $(".prenews");
    $slider = $el.find('.prenews__slider');

    $el.imagesLoaded({background: true}).always(function () {
      $slider.slick({
        arrows: false,
        dots: false,
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        autoplay: true,
        autoplaySpeed: 2000,
        vertical: false,
        draggable: false,
        responsive: [, {
          breakpoint: 1024,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2
          }
        }, {
          breakpoint: 600,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
        ]
      });
    });
  };
};