<?php

    namespace Inc\Modules\LatestNews;

    class Site
    {
        public $core;

        public function __construct($object)
        {
            $this->core = $object;
            $this->core->tpl->set('latestNews',$this->showLatestNews());           
        }

        private function showLatestNews()
        {
            $this->getPosts();
            return $this->core->tpl->draw(MODULES.'/latestnews/view/template.html');
        }

        private function getPosts()
        {
            //Get module parameters
            $parameters = $this->core->db('latestnews')->toArray();

            //Create parameters vars    
            foreach($parameters as $row)
                $parameters[$row['field']] = $row['value'];    

            //Load posts 
            $rows = $this->core->db('blog')->where('status', '=', 2)->limit($parameters['amount'])->desc('published_at')->toArray();

            $assign = [
                'posts' => []
            ];

            if (!empty($rows)) {       
                foreach ($rows as $row) {

                    //Author
                    $row['author'] = $this->core->db('users')->where('id', $row['user_id'])->oneArray();

                    //Url
                    $row['url'] = url('blog/post/'.$row['slug']);

                    //Photo url
                    $row['cover_url'] = url(UPLOADS.'/blog/'.$row['cover_photo']).'?'.$row['published_at'];

                    //Date formatting
                    $row['published_at'] = (new \DateTime(date("YmdHis", $row['published_at'])))->format( $this->core->getSettings('blog','dateformat'));
                    $keys = array_keys($this->core->lang['blog']);
                    $vals = array_values($this->core->lang['blog']);
                    $row['published_at'] = str_replace($keys, $vals, strtolower($row['published_at']));

                    //Cut content
                    $row['content'] = mb_strimwidth(strip_tags($row['content']), 0, $parameters['cut'] + 3, "...", "utf-8");                

                    //Parameters
                    $row['param_img'] = $parameters['images'];
                    $row['param_author'] = $parameters['author'];
                    $row['param_date'] = $parameters['date'];
                    $row['param_text'] = $parameters['text'];
                    $row['param_title'] = $parameters['title'];

                    $assign['posts'][$row['id']] = $row;
                }

                $this->core->tpl->set('latestnews',$assign);
            }

            if($parameters['custom'] == 'yes') {
                $this->core->addCSS(url(MODULES.'/latestnews/view/template.css'));
            }
        }
    }