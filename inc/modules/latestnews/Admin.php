<?php

    namespace Inc\Modules\LatestNews;

    class Admin
    {
        public $core;

        public function __construct($object)
        {
            $this->core = $object;
        }

        public function navigation()
        {
            return [
                $this->core->lang['general']['settings'] => 'general',
            ];
        }

        public function general()
        {
            $news = $this->core->db('latestnews')->toArray();

            //Create Module vars
            foreach($news as $row)
                $news[$row['field']] = $row['value'];    

            $this->core->tpl->set('latestnews', $news);
            
            return $this->core->tpl->draw(MODULES.'/latestnews/view/admin/general.html');
        }

        public function saveGeneral()
        {
            unset($_POST['save']);
            if(checkEmptyFields(array_keys($_POST), $_POST))
            {
                $this->core->setNotify('failure', $this->core->lang['general']['empty_inputs']);
                redirect(url([ADMIN, 'latestnews', 'general']), $_POST);
            }
            else
            {
                $errors = 0;
                foreach($_POST as $field => $value)
                {
                    if(!$this->core->db('latestnews')->where('field', $field)->save(['value' => $value]))
                    $errors++;
                }

                if(!$errors)
                    $this->core->setNotify('success', $this->core->lang['settings']['save_settings_success']);
                else
                    $this->core->setNotify('failure', $this->core->lang['settings']['save_settings_failure']);

                redirect(url([ADMIN, 'latestnews', 'general']));
            }
        }
    }